﻿using UnityEngine;
using System.Collections;

public class bow : MonoBehaviour {

	public Animator anim;
	public GameObject arrowObject;
	public GameObject holder;
	public Rigidbody arrowRb;
	public AudioSource audioDraw;
	public AudioSource audioLose;
	public ConstantForce cf;
	public TrailRenderer arrowTrail;
	public float bowDirectionX;
	public float bowDirectionY;

	Vector2 firstPos;
	Vector2 currentPos;
	float xMove;
	float yMove;
	public bool drawn; // yay çekilme başladı
	bool launch;  // yay yeterince gerildi, ok atılmaya hazır
	bool shoot;  // ok atıldı
	int drawMove;  // kamera yayı takip ediyo
	public bool bowMove; // ok atıldıktan sonra kamera oynaması
	float drawTime;
	float directionTime;
	arrow arrowScript;

	void Start () {
		drawn = false;
		launch = false;
		bowMove = true;
		firstPos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
		bowDirectionX = Random.Range (-0.001f, 0.001f);
		bowDirectionY = Random.Range (-0.001f, 0.001f);
		arrowScript = arrowObject.GetComponent<arrow> ();
	}

	void Update () {
		if (Input.GetMouseButton (0) && drawn && arrowObject.transform.localPosition.x > -5 && bowMove) { // yayı çekerken ve çekmişke
			arrowObject.transform.localPosition = new Vector3 (arrowObject.transform.localPosition.x - 0.1f, arrowObject.transform.localPosition.y, arrowObject.transform.localPosition.z);
		}
		if (!Input.GetMouseButton (0) && !launch && arrowObject.transform.localPosition.x < -3) { // yayı tam çekmeden bırakınca
			arrowObject.transform.localPosition = new Vector3 (arrowObject.transform.localPosition.x + 0.1f, arrowObject.transform.localPosition.y, arrowObject.transform.localPosition.z);
			drawMove = 0;
		}
		if (Input.GetMouseButtonDown (0) && !drawn && GameManager.arrowCount > 0) { // yayı çek
			arrowScript.taken = false;
			anim.SetTrigger ("draw");
			audioDraw.Play ();
			drawn = true;
			drawTime = Time.timeSinceLevelLoad;
			directionTime = Time.timeSinceLevelLoad;
			drawMove = 1;
		}
		if (Input.GetMouseButtonUp (0) && drawn) { // yayı bırak
			if (launch) { // yay yeterince gerildi, ok atılacak
				arrowObject.transform.parent = null;
				bowMove = false;
				anim.SetTrigger ("lose");
				launch = false;
				audioLose.Play ();
				arrowRb.AddRelativeForce (new Vector3 (0, -2000, 0));
				arrowRb.useGravity = true;
				cf.force = new Vector3(GameManager.wind, 0,0); //rüzgar

				Invoke ("animReset", 0.24f);
			} else { // yay yeterince gerilmedi, ok atılmayacak
				anim.SetTrigger ("notlaunch");
				drawn = false;
			}
		}

		if (drawn) {
			if (holder.transform.position.x + bowDirectionX / 20 < 0.4f && holder.transform.position.x + bowDirectionX / 20 > -0.4f && holder.transform.position.y + bowDirectionY / 20 < 1.8f && holder.transform.position.y + bowDirectionY / 20 > 1.4f) // yay çok uzaklaşmasın
				holder.transform.position = Vector3.MoveTowards (holder.transform.position, new Vector3 (holder.transform.position.x + bowDirectionX, holder.transform.position.y + bowDirectionY, holder.transform.position.z), Time.deltaTime / 8f);
			else {
				directionTime = Time.timeSinceLevelLoad;
				bowDirectionX = Random.Range (-5, 5);
				bowDirectionY = Random.Range (-5, 5);
			}
			if (Time.timeSinceLevelLoad - directionTime > 1.2f) {
				directionTime = Time.timeSinceLevelLoad;
				bowDirectionX = Random.Range (-5, 5);
				bowDirectionY = Random.Range (-5, 5);
			}
		} 
		 
		if(!drawn)
			drawTime = Time.timeSinceLevelLoad;


		//Yayı çekerken yayın yaklaşması ve okun pozisyonunun belirlenmesi
		if (drawMove == 1) {
				transform.localEulerAngles = Vector3.Lerp (transform.localEulerAngles, new Vector3 (0, 270, 0), Time.deltaTime * 3);
			transform.localPosition = Vector3.Lerp (transform.localPosition, new Vector3 (0.322f, 2.64f, 1.51f), Time.deltaTime * 3);
			if (Time.timeSinceLevelLoad - directionTime > 1.2f) {
				drawMove = -1;
			}
		} 
		else if (drawMove == 0) {
				transform.localEulerAngles = Vector3.Lerp (transform.localEulerAngles, new Vector3 (45, 270, 0), Time.deltaTime * 3);
			transform.localPosition = Vector3.Lerp (transform.localPosition, new Vector3 (0.4f, 1.9f, 2.7f), Time.deltaTime * 3);
		}

		if (Time.timeSinceLevelLoad - drawTime > 0.7f && drawn)// yayı ful çekmeden atamamak
			launch = true;

		currentPos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
		yMove = (currentPos.x - firstPos.x) / 5;
		xMove = (firstPos.y - currentPos.y) / 5;
		firstPos = currentPos;

		// ekrana iki ayrı uzak dokunmada bir anda yayın çok oynamaması için
		if (xMove > 30)
			xMove = 0;
		else if (xMove < -30)
			xMove = 0;
		if (yMove > 30)
			yMove = 0;
		else if (yMove < -30)
			yMove = 0;

		if (bowMove) { 
			arrowTrail.Clear ();
			// sağ sol bakış açısını sınırlandırmak için
			if (holder.transform.localEulerAngles.y + (yMove / 4) > 25 && holder.transform.localEulerAngles.y < 180)
				yMove = (25 - holder.transform.localEulerAngles.y) * 4;
			else if (holder.transform.localEulerAngles.y + (yMove / 4) < 335 && holder.transform.localEulerAngles.y > 180)
				yMove = (-25 - holder.transform.localEulerAngles.y) * 4;
			//aşağı yukarı bakış açısını sınırlandırmak için
			if (holder.transform.localEulerAngles.x + (xMove / 4) > 25 && holder.transform.localEulerAngles.x < 180)
				xMove = (25 - holder.transform.localEulerAngles.x) * 4;
			else if (holder.transform.localEulerAngles.x + (xMove / 4) < 335 && holder.transform.localEulerAngles.x > 180)
				xMove = (-25 - holder.transform.localEulerAngles.x) * 4;

			// etrafa bakmak için
			holder.transform.localEulerAngles = new Vector3 (holder.transform.localEulerAngles.x + xMove / 4, holder.transform.localEulerAngles.y + yMove / 4, 0);
		}
	}

	void animReset(){
		anim.SetTrigger ("reset");
		drawMove = 0;

	}
}
