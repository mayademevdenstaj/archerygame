﻿using UnityEngine;
using UnityEngine.UI;
//using UnityEditor.Animations;
using System.Collections;

public class arrow : MonoBehaviour {

	public Rigidbody rb;
	public ConstantForce cf;
	public AudioSource audioHit;
	Vector3 initialPos;
	public GameObject arrowPrefab;
	public GameObject bow;
	public bow bowScript;
	public TrailRenderer arrowTrail;
	public Text misssHitText;
	public Animator anim;
	public GameManager gm;
	public bool taken; //okun birden fazla sayılmamsı için. Large+medium+small
	public GameObject holder;

	bool cameraAngle;

	void Start () {
		//shotView = false;
		rb.centerOfMass = new Vector3 (0f, -2f, 0f);
		initialPos = new Vector3 (-3f, 0.3f, -0.23f);
		taken = false;
		cameraAngle = false;
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "camera") {
			Camera.main.transform.parent = col.transform;
			Camera.main.transform.localPosition =  new Vector3(2.6f, 5, -9);
			Camera.main.transform.localEulerAngles = new Vector3 (10, -16, 0);
			Camera.main.transform.parent = holder.transform;
			cameraAngle = true;
			Invoke ("focusEnd", 1f);
		}

		if (col.gameObject.tag == "baloon" && !taken) {
			hitEvents (col.gameObject, 50);
			arrowBack();
		}
		else if (col.gameObject.tag == "smallest" && !taken) {
			hitEvents (col.gameObject, 200);
			arrowBack();
		}
		else if (col.gameObject.tag == "small" && !taken) {
			hitEvents (col.gameObject, 100);
			arrowBack();
		}
		else if (col.gameObject.tag == "medium" && !taken) {
			hitEvents (col.gameObject, 50);
			arrowBack();
		}
		else if (col.gameObject.tag == "large" && !taken) {
			hitEvents (col.gameObject, 25);
			arrowBack();
		}
		else if (col.gameObject.tag == "miss") {
			misssHitText.text = "Miss!";
			arrowBack ();
		}
		else if (col.gameObject.tag == "ground") {
			audioHit.Play ();
			misssHitText.text = "Miss!";
			arrowBack ();
		}
	}

	void focusEnd()
	{
		Camera.main.transform.localPosition = new Vector3 (0.269771f, 2.731774f, 0.3077202f);
		Camera.main.transform.localEulerAngles = new Vector3 (0, 0, 0);
		bowScript.bowMove = true;
		cameraAngle = false;
	}

	void hitEvents(GameObject col, int point)
	{
		taken = true;
		audioHit.Play ();
		misssHitText.text = point.ToString () + " Point!";
		gm.score += point;

		GameObject obj;
		obj = Instantiate (arrowPrefab, new Vector3 (this.transform.position.x, this.transform.position.y, this.transform.position.z), Quaternion.Euler (this.transform.eulerAngles.x, this.transform.eulerAngles.y, this.transform.eulerAngles.z)) as GameObject;
		obj.transform.Translate (Vector3.Cross (Vector3.left, Vector3.forward) * 1.5f);

		if (col.gameObject.tag == "baloon") {
			obj.transform.parent = col.transform;
			col.gameObject.GetComponent<Baloon> ().isShot = true;
		} 
		else {
			obj.transform.parent = col.transform.parent.transform.parent;
			col.gameObject.GetComponent<target> ().isShot = true;
			col.transform.parent.transform.parent.gameObject.GetComponent<SphereCollider> ().enabled = false;
		}
	}

	void arrowBack()
	{
		GameManager.arrowCount--;
		transform.parent = bow.transform;
		this.transform.localPosition = initialPos;
		transform.localEulerAngles = new Vector3 (0, 180, 270);

		cf.force = new Vector3(0, 0,0); //rüzgar
		rb.useGravity = false;
		rb.velocity = new Vector3(0,0,0);
		rb.angularVelocity = new Vector3 (0, 0, 0);

		arrowTrail.Clear ();
		arrowTrail.Clear();
		bowScript.drawn = false;
		anim.SetTrigger ("start");
		bowScript.drawn = false;

		if(!cameraAngle)
			bowScript.bowMove = true;	
	
	}
}
