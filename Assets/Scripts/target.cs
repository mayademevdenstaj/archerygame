﻿using UnityEngine;
using System.Collections;

public class target : MonoBehaviour {

	public bool isShot;
	public bool move;
	public GameObject targetParent;
	public float speed = 0.1f;

	Vector3 initialPos;
	bool forward;

	void Start () {
		isShot = false;
		initialPos = targetParent.transform.position;
		forward = true;
	}
	
	void Update () {
		if(isShot)
		{
			

			targetParent.transform.eulerAngles = new Vector3 (targetParent.transform.eulerAngles.x+8, targetParent.transform.eulerAngles.y, targetParent.transform.eulerAngles.z);
			if (targetParent.transform.eulerAngles.x > 85) {
				isShot = false;
				if (move)
					foreach (Transform child in targetParent.transform.GetChild(0)) {
						child.gameObject.GetComponent<target> ().move = false;
						child.gameObject.GetComponent<MeshCollider>().enabled = false;
					}
			}
		}
		if (move) {
			if(forward)
				targetParent.transform.position = new Vector3(targetParent.transform.position.x + speed, initialPos.y, initialPos.z);
			else
				targetParent.transform.position = new Vector3(targetParent.transform.position.x - speed, initialPos.y, initialPos.z);

			if (targetParent.transform.position.x > initialPos.x + 8.4f)
				forward = false;
			else if (targetParent.transform.position.x < initialPos.x + 0.1f)
				forward = true;
				
		}
	}


}
