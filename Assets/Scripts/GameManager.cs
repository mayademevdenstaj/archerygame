﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour {

	public Slider windSlider;
	public Text windText;
	public static float wind;
	public Text scoreText;
	public Text timeText;
	public int score;
	public static int arrowCount;
	public Text arrowText;
	public GameObject gameOverMenu;

	void Start () {
		arrowCount = 10;
		Time.timeScale = 1;
	}
	
	void Update () {
		if (windSlider.value == 0)
			wind = -2;
		else if (windSlider.value == 1)
			wind = -1;
		else if (windSlider.value == 2)
			wind = 0;
		else if (windSlider.value == 3)
			wind = 1;
		else if (windSlider.value == 4)
			wind = 2;

		windText.text = "Wind: " + wind.ToString ();

		if ((60 - (int)Time.timeSinceLevelLoad) > 0)
			timeText.text = "Time: " + (60 - (int)Time.timeSinceLevelLoad).ToString ();
		else if ((60 - (int)Time.timeSinceLevelLoad) == 0)
			gameOver ();

		scoreText.text = "Score: " + score.ToString ();
		arrowText.text = arrowCount.ToString();
		if (arrowCount == 0)
			gameOver();
	}

	void gameOver()
	{
		Time.timeScale = 0;
		gameOverMenu.SetActive (true);
	}

	void win()
	{
		gameOverMenu.SetActive (true);
	}

	public void restart()
	{
		SceneManager.LoadScene (0);
	}
}
