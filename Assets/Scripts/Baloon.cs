﻿using UnityEngine;
using System.Collections;

public class Baloon : MonoBehaviour {

	public float speed;
	ConstantForce cf;
	public bool flyUp;
	Vector3 startCoord;
	public bool isShot;

	void Start()
	{
		cf = this.GetComponent<ConstantForce> ();
		flyUp = true;
		isShot = false;
		startCoord = transform.position;
	}

	void Update () {
		
		if (isShot) {
			transform.localScale = new Vector3 (transform.localScale.x-4, transform.localScale.y-6, transform.localScale.z);
			if (transform.localScale.y < 0)
				Destroy (gameObject);
		} 
		else {
			if (GameManager.wind == 0 && speed == 0) { //rüzgar yoksa ve hareketsizse
				if (flyUp) {
					transform.position = Vector3.Lerp (transform.position, new Vector3 (startCoord.x, startCoord.y + 0.5f, startCoord.z), 0.03f);
				} else {
					transform.position = Vector3.Lerp (transform.position, startCoord, 0.03f);
				}
				
				if (transform.position.y >= startCoord.y + 0.4f)
					flyUp = false;
				else if (transform.position.y <= startCoord.y + 0.1f)
					flyUp = true;
			} else { //rüzgarlı ve hareketli
				transform.position = new Vector3 (transform.position.x, transform.position.y + speed, transform.position.z);
				cf.force = new Vector3 (GameManager.wind / 35, 0, 0); //rüzgar
				transform.localEulerAngles = new Vector3 (90 + GameManager.wind * 5, 90, 0);
		
			}
		}
	}
}
